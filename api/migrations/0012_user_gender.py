# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2018-04-13 07:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0011_accesstoken_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='gender',
            field=models.IntegerField(default=1, max_length=2),
        ),
    ]
