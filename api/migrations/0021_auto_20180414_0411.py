# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2018-04-14 04:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0020_auto_20180414_0404'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='broker_type',
            field=models.IntegerField(choices=[(1, b'personal'), (2, b'enterprise')], default=1, help_text=b'1:personal, 2:enterprise', max_length=1),
        ),
        migrations.AlterField(
            model_name='user',
            name='gender',
            field=models.IntegerField(choices=[(1, b'male'), (2, b'female')], default=1, max_length=2),
        ),
    ]
