# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2018-04-14 08:51
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0025_auto_20180414_0847'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='ward',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='api.Ward'),
        ),
    ]
