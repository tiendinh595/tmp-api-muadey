# -*- coding: utf-8 -*-

from datetime import datetime
import time
from django.conf import settings
from PIL import Image
import PIL
import os


def generate_thumbnail(file, resize=(350, 197), is_watermark=False):
    try:
        now = datetime.now()
        relative_path = '{}/{}/{}/{}'.format(settings.UPLOAD_DIR, now.year, now.month, now.day)

        dir = '{}/{}/{}/{}/{}'.format(settings.BASE_DIR, settings.UPLOAD_DIR, now.year, now.month, now.day)
        if os.path.isdir(dir) == False:
            print dir
            os.makedirs(dir)

        file_name = str(file).split('/')[-1]
        url_file = '{}/{}'.format(relative_path, file_name)
        absolute_path = '{}/{}'.format(settings.BASE_DIR, url_file)

        thumb_name = 'thumb-{}'.format(str(file).split('/')[-1])
        url_thumb = '{}/{}'.format(relative_path, thumb_name)
        absolute_path_thumb = '{}/{}'.format(settings.BASE_DIR, url_thumb)

        image = Image.open(absolute_path)

        wpercent = (resize[0] / float(image.size[0]))
        hsize = int((float(image.size[1]) * float(wpercent)))
        image = image.resize((resize[0], hsize), Image.ANTIALIAS)
        image.save(absolute_path_thumb)
        return url_thumb, True
    except Exception as ex:
        print ex
        return None, False


def handle_uploaded_file(f, resize=None, thumb=None, is_watermark=False):
    try:
        now = datetime.now()
        relative_path = '{}/{}/{}/{}'.format(settings.UPLOAD_DIR, now.year, now.month, now.day)

        dir = '{}/{}/{}/{}/{}'.format(settings.BASE_DIR, settings.UPLOAD_DIR, now.year, now.month, now.day)
        if os.path.isdir(dir) == False:
            print dir
            os.makedirs(dir)

        extension = str(f.name).split('.')[-1]
        file_name = '{}.{}'.format(time.time(), extension)
        url_file = '{}/{}'.format(relative_path, file_name)
        absolute_path = '{}/{}'.format(settings.BASE_DIR, url_file)

        with open(absolute_path, 'wb+') as destination:
            for chunk in f.chunks():
                destination.write(chunk)

        if resize != None:
            image = Image.open(absolute_path)
            image.thumbnail(resize, Image.ANTIALIAS)
            image.save(absolute_path)

        if is_watermark == True:
            image = Image.open(absolute_path)
            image_size = image.size
            watermark = Image.open('{}/{}'.format(settings.BASE_DIR, 'storage/images/logo.png'))
            watermark_size = watermark.size
            image.paste(watermark, (image_size[0]-watermark_size[0], image_size[1]-watermark_size[1]), watermark)
            image.save(absolute_path)

        url_thumb = None
        generated_thumb = False
        if thumb != None:
            url_thumb, generated_thumb = generate_thumbnail(url_file, thumb, is_watermark)

        return url_file, True, url_thumb, generated_thumb
    except Exception as ex:
        return None, False
