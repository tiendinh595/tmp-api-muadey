# -*- coding: utf-8 -*-

from .base import *

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases
URL_API = 'http://127.0.0.1:8000'
DEBUG = True
CORS_ORIGIN_ALLOW_ALL = True

# swagger
SWAGGER_SETTINGS = {
    'exclude_url_names': [],
    'exclude_namespaces': [],
    'api_version': '1.0',
    'api_path': 'http://127.0.0.1:8000',
    'relative_paths': False,
    'enabled_methods': [
        'get',
        'post',
        'put',
        'patch',
        'delete'
    ],
    'api_key': '',
    'is_authenticated': False,
    'is_superuser': False,
    'unauthenticated_user': 'django.contrib.auth.models.AnonymousUser',
    'permission_denied_handler': None,
    'resource_access_handler': None,
    'base_path': '127.0.0.1:8000/docs',
    'info': {
        'contact': 'tiendinh595@gmail.com',
        'description': '',
        'license': '',
        'licenseUrl': '',
        'termsOfServiceUrl': '',
        'title': 'Core api build by Tien Dinh',
    },
    'doc_expansion': 'none',
}

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.mysql',
        'NAME': 'cmn_bds',
        'USER': 'root',
        'PASSWORD': 'mysql',
        'HOST': '127.0.0.1',
        'PORT': '3306',
        'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'; SET foreign_key_checks = 0;",
        },
    }
}
