# -*- coding: utf-8 -*-

from .base import *

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases
DEBUG = True
URL_API = 'https://api.muadey.com'

# swagger
SWAGGER_SETTINGS = {
    'exclude_url_names': [],
    'exclude_namespaces': [],
    'api_version': '1.0',
    'api_path': 'https://api.muadey.com',
    'relative_paths': False,
    'enabled_methods': [
        'get',
        'post',
        'put',
        'patch',
        'delete'
    ],
    'api_key': '',
    'is_authenticated': False,
    'is_superuser': False,
    'unauthenticated_user': 'django.contrib.auth.models.AnonymousUser',
    'permission_denied_handler': None,
    'resource_access_handler': None,
    'base_path': 'api.muadey.com/docs',
    'info': {
        'contact': 'tiendinh595@gmail.com',
        'description': '',
        'license': '',
        'licenseUrl': '',
        'termsOfServiceUrl': '',
        'title': 'Core api build by Tien Dinh',
    },
    'doc_expansion': 'none',
}

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.mysql',
        'NAME': 'bds',
        'USER': 'root',
        'PASSWORD': 'bds_cmn!@#',
        'HOST': 'localhost',
        'PORT': '3306',
    }
}
