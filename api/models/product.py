# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.gis.db import models
from api.models import District, City, File
from django.utils.text import slugify
from django.db.models import Q

class ProductType(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    master = models.ForeignKey("ProductType", models.CASCADE, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    slug = models.SlugField(null=True, blank=True)
    keyword = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    def get_all_children(self):
        return ProductType.objects.filter(master=self)

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.title)

        super(ProductType, self).save(*args, **kwargs)

    class Meta:
        db_table = 'tbl_product_type'


class ProductQueryset(models.QuerySet):

    def active(self):
        return self.filter(deleted_at=None)

    def deleted(self):
        return self.filter(~Q(deleted_at=None))

    def update(self, *args, **kwargs):
        print 'update 2'
        product = self.first()
        product.price_type = int(kwargs.get('price_type'))
        product.price_from = float(kwargs.get('price_from'))
        product.price_to = float(kwargs.get('price_to'))
        return product.save(*args, **kwargs)


class ProductManager(models.Manager):

    def get_queryset(self):
        return ProductQueryset(self.model)

    def active(self):
        return self.get_queryset().active()

    def deleted(self):
        return self.get_queryset().deleted()

class Product(models.Model):

    KXD = 0
    DONG = 1
    TAY = 2
    NAM = 3
    BAC = 4
    TAY_BAC = 5
    DONG_BAC = 6
    TAY_NAM = 7
    DONG_NAM = 8
    HOME_DIRECTION = (
        (KXD, 'Không xác định'),
        (DONG, 'Đông'),
        (TAY, 'Tây'),
        (NAM, 'Nam'),
        (BAC, 'Bắc'),
        (TAY_BAC, 'Tây Bắc'),
        (DONG_BAC, 'Đông Bắc'),
        (TAY_NAM, 'Tây Nam'),
        (DONG_NAM, 'Đông nam'),
    )
    BALCONY_DIRECTION = (
        (KXD, 'Không xác định'),
        (DONG, 'Đông'),
        (TAY, 'Tây'),
        (NAM, 'Nam'),
        (BAC, 'Bắc'),
        (TAY_BAC, 'Tây Bắc'),
        (DONG_BAC, 'Đông Bắc'),
        (TAY_NAM, 'Tây Nam'),
        (DONG_NAM, 'Đông nam'),
    )

    THOA_THUAN=1
    TRAM_NGHIN=2
    TRIEU=3
    TY=4
    PRICE_TYPE = (
        (THOA_THUAN, 'Thỏa thuận'),
        (TRAM_NGHIN, 'Trăm nghìn'),
        (TRIEU, 'Triệu'),
        (TY, 'Tỷ')
    )

    PRICE_TYPE_FULL = (
        (TRAM_NGHIN, 100000),
        (TRIEU, 1000000),
        (TY, 1000000000)
    )

    user = models.ForeignKey('User', models.DO_NOTHING, blank=True, null=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    slug = models.CharField(max_length=255,null=True, blank=True)
    description = models.TextField(blank=True, null=True)
    product_type_master = models.ForeignKey('ProductType', models.DO_NOTHING, blank=True, null=True,
                                            related_name='product_type_master')
    product_type_child = models.ForeignKey('ProductType', models.DO_NOTHING, blank=True, null=True,
                                           related_name='product_type_child')
    address = models.CharField(max_length=255, blank=True, null=True)
    ward = models.ForeignKey('Ward', models.DO_NOTHING, blank=True, null=True)
    district = models.ForeignKey(District, models.DO_NOTHING, blank=True, null=True)
    city = models.ForeignKey(City, models.DO_NOTHING, blank=True, null=True)
    project = models.ForeignKey('Project', models.DO_NOTHING, blank=True, null=True)
    location = models.PointField(blank=True, null=True)  # This field type is a guess.
    price_from = models.FloatField(blank=True, null=True)
    price_to = models.FloatField(blank=True, null=True)
    price_from_full = models.FloatField(blank=True, null=True)
    price_to_full = models.FloatField(blank=True, null=True)
    price_type = models.IntegerField(blank=True, null=True, choices=PRICE_TYPE, default=THOA_THUAN)
    area = models.FloatField(blank=True, null=True)
    thumbnail = models.ForeignKey(File, related_name="thumbnail_product", default='', null=True, blank=True, on_delete=models.CASCADE)
    images = models.ManyToManyField(File, related_name='images')
    images_extra = models.ManyToManyField(File, related_name='images_extra')
    frontage_area = models.IntegerField(blank=True, null=True)
    land_with = models.IntegerField(blank=True, null=True)
    home_direction = models.IntegerField(blank=True, null=True, choices=HOME_DIRECTION, default=KXD)
    balcony_direction = models.IntegerField(blank=True, null=True, choices=BALCONY_DIRECTION, default=KXD)
    floor_number = models.IntegerField(blank=True, null=True)
    room_number = models.IntegerField(blank=True, null=True)
    toilet_number = models.IntegerField(blank=True, null=True)
    interior = models.TextField(blank=True, null=True)
    contact_name = models.CharField(max_length=100, blank=True, null=True)
    contact_phone = models.CharField(max_length=30, blank=True, null=True)
    contact_address = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True, auto_now=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    objects = ProductManager()

    class Meta:
        ordering = ["-id"]
        db_table = 'tbl_product'

    def get_balcony_direction_display(self):
        return dict(self.BALCONY_DIRECTION)[int(self.balcony_direction)]

    def get_home_direction_display(self):
        raise Exception('xxx')

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.title)

        num_multiplication = dict(self.PRICE_TYPE_FULL)
        self.price_type = int(self.price_type)
        print self.land_with

        if self.price_type in num_multiplication.keys():
            self.price_from = float(self.price_from)
            self.price_to = float(self.price_to)
            self.price_from_full = self.price_from*num_multiplication[self.price_type]
            self.price_to_full = self.price_to*num_multiplication[self.price_type]

        # print kwargs

        return super(Product, self).save(*args, **kwargs)
