# -*- coding: utf-8 -*-

from rest_framework.permissions import BasePermission


class TokenIsAuthenticated(BasePermission):
    message = u'chưa login'

    def has_permission(self, request, view):
        if request.user:
            return True
        return False

class IsAuthenticatedOrReadOnly(BaseException):

    SAFE_METHODS = ["GET"]

    def has_permission(self, request, view):
        if request.method in self.SAFE_METHODS:
            return True

        if request.user:
            return True
        return False