# -*- coding: utf-8 -*-

from rest_framework.views import APIView
from api.authentication import TokenAuthentication
from api.permissions import TokenIsAuthenticated
from api.services.common import response
from api.utils import md5
import jwt
from django.conf import settings
import random
from rest_framework.parsers import MultiPartParser, FormParser
import datetime
# model
from api.models.user import User, AccessToken

# serializer
from api.serializers.user import UserSerializer

# exception
from api.exceptions import InvalidAPIQuery
from rest_framework.serializers import ValidationError

# tasks
from api.tasks import demo

# service
from api.services.validate import Validator, ValidatorException


class Login(APIView):
    #
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (TokenIsAuthenticated,)

    def post(self, request):
        """
        Login account

        ---

        response_serializer: api.serializers.user.UserSerializer

        parameters:
            - name: username
              description:
              required: true
              type: string
              paramType: form
            - name: password
              description:
              required: true
              type: string
              paramType: form


        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """

        try:

            username, password = request.POST.get('username'), request.POST.get('password')

            validator = Validator(request.POST, {
                "username": [
                    "required"
                ],
                "password": [
                    "required"
                ]
            }, {
                "username":"Tên đăng nhập",
                "password":"Mật khẩu",
            })

            password = md5(password)
            user = User.objects.filter(username=username).first()

            if not user:
                validator.update_errors("username", "Sai tên đăng nhập")

            if user and user.password != password:
                validator.update_errors("password", "Sai mật khẩu")

            validator.fails(raise_exception=True)

            access_token, created = AccessToken.objects.get_or_create(user=user)
            access_token.key = random.randint(1000000, 999999999)
            access_token.token = jwt.encode(
                {'user_id': user.id, 'id': access_token.id, 'key': access_token.key},
                key=settings.JWT.get('secret'))
            access_token.save()

            serializer = UserSerializer(user)
            return response.success(serializer.data)
        except ValidatorException as ex:
            return response.error(ex.message, ex.errors)
        except Exception as ex:
            return response.error(ex.__str__())

#
# class Register(APIView):
#
#     def post(self, request):
#         """
#         Register account
#
#         ---
#
#         response_serializer: api.serializers.user.UserSerializer
#
#         parameters:
#             - name: name
#               description:
#               required: true
#               type: string
#               paramType: form
#             - name: username
#               description:
#               required: true
#               type: string
#               paramType: form
#             - name: password
#               description:
#               required: true
#               type: string
#               paramType: form
#             - name: re_password
#               description:
#               required: true
#               type: string
#               paramType: form
#             - name: email
#               description:
#               required: true
#               type: string
#               paramType: form
#             - name: mobile
#               description:
#               required: true
#               type: string
#               paramType: form
#             - name: birthday
#               description: 'Y-m-d'
#               required: false
#               type: string
#               paramType: form
#             - name: gender
#               description: '1: male, 2: female'
#               required: true
#               type: string
#               paramType: form
#             - name: account_type
#               description: '1: personal, 2: enterprise'
#               required: true
#               type: string
#               paramType: form
#             - name: city_id
#               description:
#               required: false
#               type: string
#               paramType: form
#             - name: district_id
#               description:
#               required: false
#               type: string
#               paramType: form
#             - name: ward_id
#               description:
#               required: false
#               type: string
#               paramType: form
#
#
#         responseMessages:
#             - code: 200
#               message: Success Json Object
#             - code: 400
#               message: '{"error" : 400 , "message" : "Not Valid"}'
#             - code: 403
#               message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
#             - code: 404
#               message: '{"error" : 404 , "message" : "Not Found"}'
#             - code: 500
#               message: '{"error" : 500 , "message" : "Internal Error"}'
#
#         consumes:
#             - application/json
#
#         produces:
#             - application/json
#         """
#
#         try:
#             rules = {
#                 "username": [
#                     "required", "unique:User,username", "max:100"
#                 ],
#                 "email": [
#                     "required", "unique:User,email", "email"
#                 ],
#                 "password": [
#                     "str", "min:4",
#                 ],
#                 "re_password": [
#                     "same:password"
#                 ],
#                 "mobile": [
#                     "required", "unique:User,mobile"
#                 ],
#                 "name": [
#                     "str", "required", "max:100"
#                 ],
#                 "gender": [
#                     "required", "in:1,2"
#                 ],
#                 "account_type": [
#                     "required", "in:1,2"
#                 ],
#                 "birthday": [
#                     "nullable", "date"
#                 ],
#                 "city_id": [
#                     "nullable", "exists:City,id"
#                 ],
#                 "district_id": [
#                     "nullable", "exists:District,id,city_id"
#                 ],
#                 "ward_id": [
#                     "nullable", "exists:Ward,id,district_id"
#                 ],
#                 "captcha": [
#                     "required",
#                     'captcha'
#                 ]
#             }
#             names = {
#                 "username":"Tên đăng nhập",
#                 "email":"Email",
#                 "password":"Mật khẩu",
#                 "re_password":"Xác nhận mật khẩu",
#                 "mobile":"Điện thoại",
#                 "name":"Họ tên",
#                 "gender":"Giới tính",
#                 "account_type":"Loại tài khoản",
#                 "birthday":"Ngày sinh",
#                 "city_id":"Tỉnh/Thành phố",
#                 "district_id":"Huyện/Quận",
#                 "ward_id":"Xã/Phường"
#             }
#             validator = Validator(request.POST, rules, names)
#             validator.fails(raise_exception=True)
#
#             data = validator.get_data_clear()
#             del data['re_password']
#             del data['captcha']
#             data['password'] = md5(data['password'])
#             user = User.objects.create(**data)
#
#             access_token, created = AccessToken.objects.get_or_create(
#                 user=user,
#                 key=random.randint(1000000, 999999999)
#             )
#             access_token.token = jwt.encode(
#                 {'user_id': user.id, 'id': access_token.id, 'key': access_token.key, },
#                 key=settings.JWT.get('secret'))
#             access_token.save()
#
#             serializer = UserSerializer(user)
#             return response.success(serializer.data, {}, "Đăng ký thành công")
#         except User.DoesNotExist:
#             return response.error(u'credential incorrect ')
#         except ValidationError as ex:
#             return response.error(ex)
#         except ValidatorException as ex:
#             return response.error(ex.message, ex.errors)
#         except Exception as ex:
#             return response.error(str(ex))

class Register(APIView):

    def post(self, request):
        """
        Register account

        ---

        response_serializer: api.serializers.user.UserSerializer

        parameters:
            - name: name
              description:
              required: true
              type: string
              paramType: form
            - name: password
              description:
              required: true
              type: string
              paramType: form
            - name: re_password
              description:
              required: true
              type: string
              paramType: form
            - name: mobile
              description:
              required: true
              type: string
              paramType: form
        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """

        try:
            rules = {
                "password": [
                    "str", "min:4",
                ],
                "re_password": [
                    "same:password"
                ],
                "mobile": [
                    "required", "unique:User,mobile"
                ],
                "name": [
                    "str", "required", "max:100"
                ],
                "captcha": [
                    "required",
                    'captcha'
                ]
            }
            names = {
                "password":"Mật khẩu",
                "re_password":"Xác nhận mật khẩu",
                "mobile":"Điện thoại",
                "name":"Họ tên",
            }
            validator = Validator(request.POST, rules, names)
            validator.fails(raise_exception=True)

            data = validator.get_data_clear()
            del data['re_password']
            del data['captcha']
            data['password'] = md5(data['password'])
            data['username'] = data['mobile']
            user = User.objects.create(**data)

            access_token, created = AccessToken.objects.get_or_create(
                user=user,
                key=random.randint(1000000, 999999999)
            )
            access_token.token = jwt.encode(
                {'user_id': user.id, 'id': access_token.id, 'key': access_token.key, },
                key=settings.JWT.get('secret'))
            access_token.save()

            serializer = UserSerializer(user)
            return response.success(serializer.data, {}, "Đăng ký thành công")
        except User.DoesNotExist:
            return response.error(u'credential incorrect ')
        except ValidationError as ex:
            return response.error(ex)
        except ValidatorException as ex:
            return response.error(ex.message, ex.errors)
        except Exception as ex:
            return response.error(str(ex))


class Profile(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def get(self, request):
        """
        get profile

        ---

        response_serializer: api.serializers.user.UserSerializer

        parameters:
            - name: Authorization
              description: access token
              required: true
              type: string
              paramType: header

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """

        try:
            # print '-------'
            # print demo.add.delay(2, 3)
            # print demo.add.name
            # print '-------'
            # user = User.objects.get(pk=request.user)
            # user = User.objects.get(pk=1)
            # access_token = AccessToken.objects.filter(toke)
            user = User.objects.get(id=request.auth)
            serializer = UserSerializer(user)
            return response.success(serializer.data)
        except User.DoesNotExist:
            return response.error(u'credential incorrect ')
        except Exception as ex:
            return response.error(ex)

    def put(self, request):
        """
        update profile

        ---

        response_serializer: api.serializers.user.UserSerializer

        parameters:
            - name: Authorization
              description: access token
              required: true
              type: string
              paramType: header
            - name: name
              description:
              required: true
              type: string
              paramType: form
            - name: birthday
              description:
              required: false
              type: string
              paramType: form
            - name: gender
              description:
              required: true
              type: string
              paramType: form
            - name: city_id
              description:
              required: false
              type: string
              paramType: form
            - name: district_id
              description:
              required: false
              type: string
              paramType: form
            - name: ward_id
              description:
              required: false
              type: string
              paramType: form
            - name: address
              description:
              required: false
              type: string
              paramType: form
            - name: mobile
              description:
              required: true
              type: string
              paramType: form
            - name: email
              description:
              required: true
              type: string
              paramType: form
            - name: facebook
              description:
              required: true
              type: string
              paramType: form
            - name: avatar
              description:
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """

        try:

            validator = Validator(request.POST, {
                'name': [
                    'required'
                ],
                'email': [
                    'required',
                    'email'
                ],
                # 'mobile': [
                #     'required'
                # ],
                'birthday': [
                    'nullable',
                    'date'
                ],
                'gender': [
                    'required',
                    'in:1,2'
                ],
                'facebook': [
                    'nullable',
                    'str'
                ],
                "city_id": [
                    "nullable", "exists:City,id"
                ],
                "district_id": [
                    "nullable", "exists:District,id,city_id"
                ],
                "ward_id": [
                    "nullable", "exists:Ward,id,district_id"
                ],
                'address': [
                    'nullable',
                    'max:255'
                ],
                'avatar': [
                    'nullable',
                    'max:255'
                ],
                # 'facebook': ''
            }, {
                "email":"Email",
                "mobile":"Điện thoại",
                "name":"Họ tên",
                "gender":"Giới tính",
                "birthday":"Ngày sinh",
                "city_id":"Tỉnh/Thành phố",
                "district_id":"Huyện/Quận",
                "ward_id":"Xã/Phường",
                "address":"Địa chỉ",
            })
            validator.fails(raise_exception=True)
            data = validator.get_data_clear(['mobile'])

            User.objects.filter(id=request.auth).update(**data)
            user = User.objects.get(id=request.auth)
            serializer = UserSerializer(user)
            return response.success(serializer.data, {}, "Cập nhật thành công")
        except User.DoesNotExist:
            return response.error(u'')
        except ValidatorException as ex:
            return response.error(ex.message, ex.errors)
        except Exception as ex:
            return response.error(ex)


class ChangePassword(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def put(self, request):
        """
        change password

        ---

        response_serializer: api.serializers.user.UserSerializer

        parameters:
            - name: Authorization
              description: access token
              required: true
              type: string
              paramType: header
            - name: old_password
              description:
              required: true
              type: string
              paramType: form
            - name: new_password
              description:
              required: true
              type: string
              paramType: form
            - name: re_new_password
              description:
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """

        try:

            validator = Validator(request.POST, {
                'old_password': [
                    'required'
                ],
                'new_password': [
                    'required'
                ],
                're_new_password': [
                    'same:new_password'
                ]
            }, {
                "old_password":"Mật khẩu cũ",
                "new_password":"Mật khẩu mới",
                "re_new_password":"Nhập lại mật khẩu mới",
            })

            user = User.objects.get(id=request.auth)

            if user and user.password != md5(request.POST.get('old_password')):
                validator.update_errors("old_password", "Mật khẩu cũ không đúng")

            validator.fails(raise_exception=True)


            user.password = md5(request.POST.get('new_password'))
            user.save()

            serializer = UserSerializer(user)
            return response.success(serializer.data, {}, "Thay đổi mật khẩu thành công")
        except User.DoesNotExist:
            return response.error(u'')
        except ValidatorException as ex:
            return response.error(ex.message, ex.errors)
        except Exception as ex:
            return response.error(ex)
