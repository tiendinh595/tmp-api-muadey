# -*- coding: utf-8 -*-

from rest_framework.views import APIView
from api.services.common import response
from api.tasks.mail import send_mail_contact
from api.services.validate import Validator, ValidatorException


class MailView(APIView):
    #
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (TokenIsAuthenticated,)

    def post(self, request):
        """
        send mail contact

        ---

        parameters:
            - name: name
              description:
              required: true
              type: string
              paramType: form
            - name: email
              description:
              required: true
              type: string
              paramType: form
            - name: content
              description:
              required: true
              type: string
              paramType: form


        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """

        try:
            validator = Validator(request.POST, {
                "name": [
                    "required", "max:100"
                ],
                "email": [
                    "required", "email"
                ],
                "content": [
                    "required", "max:500"
                ]
            })
            validator.fails(raise_exception=True)
            data = validator.get_data_clear()
            data['content'] = """
            Bạn có thư liên lạc từ {}.\n
            {}
            """.format(data['name'], data['content'])
            send_mail_contact.delay(data['email'], data['content'])

            return response.success(message='gửi mail liên hệ thành công')
        except ValidatorException as ex:
            return response.error(ex.message, ex.errors)
        except Exception as ex:
            return response.error(ex.__str__())
