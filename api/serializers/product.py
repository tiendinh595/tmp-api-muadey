# -*- coding: utf-8 -*-
from rest_framework import serializers
from api.models import City, District, Ward, Product, ProductType
from api.serializers.user import UserProductSerializer
from api.serializers.location import WardSerializer, DistrictSerializer, CitySerializer
from api.serializers.file import FileSerializer
import json
import datetime
import locale


def number_format(num, places=0):
    num = float(num)
    return locale.format("%.*f", (places, num), True)


class ProductTypeItemSerializer(serializers.ModelSerializer):
    childs = serializers.SerializerMethodField()

    def get_childs(self, product_type):
        return ProductTypeItemSerializer(product_type.get_all_children(), many=True).data

    class Meta:
        model = ProductType
        fields = ('id', 'name', 'master_id', 'childs', 'slug')


class ProductTypeDetailSerializer(serializers.ModelSerializer):
    childs = serializers.SerializerMethodField()
    master = ProductTypeItemSerializer()

    def get_childs(self, product_type):
        return ProductTypeItemSerializer(product_type.get_all_children(), many=True).data

    class Meta:
        model = ProductType
        fields = ('id', 'name', 'master', 'keyword', 'description', 'childs', 'master', 'slug')


class ProductDetailSerializer(serializers.ModelSerializer):
    user = UserProductSerializer()
    ward = WardSerializer()
    district = DistrictSerializer()
    city = CitySerializer()
    product_type_master = ProductTypeItemSerializer()
    product_type_child = ProductTypeItemSerializer()
    images = FileSerializer(many=True)
    images_extra = FileSerializer(many=True)
    thumbnail = FileSerializer()
    lat = serializers.SerializerMethodField()
    lng = serializers.SerializerMethodField()
    price_from_format = serializers.SerializerMethodField()
    price_to_format = serializers.SerializerMethodField()
    price_type_name = serializers.SerializerMethodField()

    def get_lat(self, product):
        return product.location.x

    def get_lng(self, product):
        return product.location.y

    def get_price_from_format(self, product):
        return number_format(product.price_from)

    def get_price_to_format(self, product):
        return number_format(product.price_to)

    def get_price_type_name(self, product):
        return dict(Product.PRICE_TYPE)[int(product.price_type)]

    class Meta:
        model = Product
        fields = ('id', 'user', 'title', 'description', 'product_type_master', 'product_type_child', 'address', 'ward',
                  'district', 'city', 'project', 'location', 'price_from', 'price_to', 'price_from_format',
                  'price_to_format', 'price_type', 'area', 'images', 'images_extra', 'thumbnail',
                  'frontage_area', 'land_with', 'home_direction', 'balcony_direction', 'floor_number', 'room_number',
                  'toilet_number', 'interior', 'contact_name', 'contact_phone', 'contact_address', 'lat', 'lng',
                  'created_at',
                  'updated_at', 'slug', 'city_id', 'district_id', 'ward_id', 'product_type_child_id', 'price_type', 'price_type_name')


class ProductItemSerializer(serializers.ModelSerializer):
    ward = WardSerializer()
    district = DistrictSerializer()
    city = CitySerializer()
    images = FileSerializer(many=True)
    thumbnail = FileSerializer()
    lat = serializers.SerializerMethodField()
    lng = serializers.SerializerMethodField()
    price_from_format = serializers.SerializerMethodField()
    price_to_format = serializers.SerializerMethodField()
    price_type_name = serializers.SerializerMethodField()
    created_at = serializers.SerializerMethodField()

    def get_price_type_name(self, product):
        return dict(Product.PRICE_TYPE)[product.price_type]

    def get_lat(self, product):
        return product.location.y

    def get_lng(self, product):
        return product.location.x

    def get_price_from_format(self, product):
        return number_format(product.price_from)

    def get_price_to_format(self, product):
        return number_format(product.price_to)

    def get_images(self, product):
        try:
            return json.loads(product.images)
        except:
            return []

    def get_created_at(self, product):
        try:
            return product.created_at.strftime("%d-%m-%Y %H:%I:%S")
        except:
            return ''

    class Meta:
        model = Product
        fields = ('id', 'title', 'address', 'ward', 'district', 'city', 'project', 'location', 'price_from', 'price_to'
                  , 'price_from_format', 'price_to_format', 'price_type', 'area', 'images', 'thumbnail', 'floor_number',
                  'room_number', 'lat', 'lng', 'slug', 'price_type', 'price_type_name', 'created_at')
