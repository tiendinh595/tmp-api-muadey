# -*- coding: utf-8 -*-

from rest_framework import serializers

class DemoSerializer(serializers.Serializer):
    foo = serializers.CharField(max_length=100)